import asdf
from pathlib import Path
from copy import deepcopy
from datetime import datetime, timedelta
from surtseygeotherm import dataloc, software_entry, SRATE, versionmap

THIS_FILES_NAME = f"{Path(__file__).stem}.py"
COLS = ["t", "mass"] + [f"T{i}" for i in range(16)] + ["CJC03", "CJC47", "CJC811", "CJC1215"]
UNITS = {
    "CJC03": "°C", "CJC1215": "°C", "CJC47": "°C", "CJC811": "°C", "T0": "°C",
    "T1": "°C", "T10": "°C", "T11": "°C", "T12": "°C", "T13": "°C", "T14": "°C",
    "T15": "°C", "T2": "°C", "T3": "°C", "T4": "°C", "T5": "°C", "T6": "°C",
    "T7": "°C", "T8": "°C", "T9": "°C", "mass": "kg", "t": "s"
}
VINFO = deepcopy(versionmap['runlevels'][1])
VINFO['creator-files'] = [THIS_FILES_NAME]


def get_records() -> list:
    asdfloc = dataloc
    recs = []
    for el in asdfloc.glob("*.asdf"):
        el = el.name.rsplit("_", maxsplit=1)
        if len(el) > 1:
            left, right = el
            if left not in recs:
                if left[:2].isnumeric():
                    pass
                else:
                    recs.append(left)
    return recs


def create_rl1_asdf():
    recs = get_records()
    for rec in recs:
        with asdf.open(dataloc / f"{rec}_t.asdf") as af:
            enddt: datetime = af['end_time']
            ln = af['t'].shape[0]
            startdt: datetime = enddt - timedelta(seconds=ln / SRATE)
        af = asdf.AsdfFile()
        af["record"] = rec
        af["raw"] = {col: {"$ref": f"{rec}_{col}.asdf#{col}"} for col in COLS}
        af["raw"]["columns"] = COLS
        af["raw"]["units"] = UNITS
        af["start_time"] = startdt
        af["end_time"] = enddt
        af["length"] = ln
        af["versioninfo"] = VINFO
        af.add_history_entry(
            description="Create this runlevel 1 file.",
            software=software_entry
        )
        af.write_to(dataloc / f"{rec}.asdf", all_array_storage="inline")
        af.close()
        print(f"Done with {rec}.")


if __name__ == "__main__":
    create_rl1_asdf()
