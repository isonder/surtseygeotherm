import pandas as pd
import numpy as np
from numpy.typing import NDArray
import asdf
from typing import Union, Callable
from datetime import datetime, timedelta
from surtseygeotherm import TOPLEVEL_SRC, SRATE

str_or_dt = Union[str, datetime]
TCOLS = [f'T{i}' for i in range(16)]
DEFAULTCOLS = ['mass'] + TCOLS
ALLCOLS = DEFAULTCOLS + ['CJC03', 'CJC47', 'CJC811', 'CJC1215']
times_and_recs: pd.DataFrame = pd.DataFrame()


def setup_times():
    """Load start and end times and corresponding record names from toplevel
    asdf.
    """
    global times_and_recs
    af = asdf.open(TOPLEVEL_SRC)
    tl = af['timeline']
    df = pd.DataFrame(index=pd.RangeIndex(len(tl['record'])))
    df['start_time'] = tl['start_time']
    df['end_time'] = tl['end_time']
    df['record'] = tl['record']
    times_and_recs = df


def consistent_timerange(func: Callable):

    def make_consistent(start: str_or_dt, end: str_or_dt, rtype: str, **kwds):
        if isinstance(start, str):
            start = datetime.fromisoformat(start)
        if isinstance(end, str):
            end = datetime.fromisoformat(end)
        assert end > start, \
            (f"Cannot search for time span which ended before it started. "
             f"{start=}, {end=}")
        rtype = rtype.lower()
        if rtype not in ('df', 'ndarray', 'list'):
            raise ValueError(
                f"rtype has to be one of 'df', 'ndarray', but got {rtype=}")
        return func(start, end, rtype, **kwds)
    make_consistent.__doc__ = func.__doc__
    return make_consistent


@consistent_timerange
def records_from_timespan(
        start: str_or_dt, end: str_or_dt, rtype: str = 'list'
) -> Union[list, pd.DataFrame]:
    """Return a list of all records that hold data of a given time span.

    Parameters
    ----------
    start : str or datetime
        Start date and time.
    end : str or datetime
        End date and time.
    rtype : str, optional, default is 'list'
        Return type. One of 'list', 'df' (dataframe).

    Returns
    -------

    """
    if len(times_and_recs) == 0:
        setup_times()
    datastart = times_and_recs.iloc[0]['start_time']
    dataend = times_and_recs.iloc[-1]['end_time']
    if start > dataend or end < datastart:
        raise ValueError(
            f"Requested timespan lies outside the data's time range.\n"
            f"  Requested start: {start.isoformat(' ')}, "
            f"start of data: {datastart.to_pydatetime().isoformat(' ')}\n"
            f"  Requested end:   {end.isoformat(' ')}, "
            f"end of data: {dataend.to_pydatetime().isoformat(' ')}\n"
        )
    if start < datastart:
        start = datastart.to_pydatetime()
    if end > dataend:
        end = dataend.to_pydatetime()
    idx = times_and_recs['start_time'] > start
    mn = times_and_recs.index[idx].min()
    idx = np.logical_and(idx, times_and_recs['end_time'] < end)
    if np.any(idx):  # Case that [start, end] spans across several records
        mn, mx = times_and_recs.index[idx].min(), times_and_recs.index[idx].max()
        # print(f"records_from_timespan: {mn=}, {mx=}")
        idx[[mn - 1, mx + 1]] = True
    else:            # Case that [start, end] lie in one record
        idx[mn - 1] = True
    rtype = rtype.lower()
    if rtype == 'list':
        return times_and_recs.loc[idx, 'record'].to_list()
    elif rtype == 'df':
        return times_and_recs.loc[idx]


@consistent_timerange
def load(
        start: datetime, end: datetime, rtype: str = "df", **kwds
) -> Union[pd.DataFrame, NDArray, tuple]:
    """Load data from one or multiple records from disk, based on specified
    time interval.

    Parameters
    ----------
    start : datetime or iso-formatted datetime string
        Requested start date and time.
    end : datetime or iso-formatted datetime string
        Requested end date and time.
    rtype: str
        Return type. One of `df` (a pandas dataframe, the default), `ndarray`
        (a numpy array).
    cols : list, optional
        Columns to read from disk. The default is mass plus all temperature
        channels except cold junction compensation.
    index : str, optional
        One of `t` (default), `range`. Type of index to use for the dataframe
        return type. `t` uses the time column as index, `range` a running
        integer.

    Returns
    -------
        pd.DataFrame or NDArray
    Loaded data.
    """
    cols = DEFAULTCOLS if 'cols' not in kwds else kwds['cols']
    index = 't' if 'index' not in kwds.keys() else kwds['index']
    if index not in ('t', 'range'):
        raise ValueError(
            f"index parameter must be one of 't', 'range', but got {index=}.")
    rat: pd.DataFrame = records_from_timespan(start, end, rtype='df')
    ratidx = rat.index.values
    recs = rat['record'].to_list()
    start_times = rat['start_time'].to_list()
    # recs: list = records_from_timespan(start, end, rtype='list')
    af = asdf.open(TOPLEVEL_SRC)
    # determine start index
    #  start record
    recstart: datetime = af['timeline']['start_time'][ratidx[0]]
    print(f"{recstart=}, {start=}")
    t_off = (start - recstart).total_seconds()
    offset = int(SRATE * t_off)
    # For each record, determine start and end index to read.
    ln, tt = [], []
    if len(recs) == 1:
        rel_tend = (end - start_times[0]).total_seconds()
        relend = int(SRATE * rel_tend)
        ln.append([offset, relend])
        tt.append(np.arange(relend - offset) / SRATE)
    else:
        relend = af['timeline']['length'][ratidx[0]]
        ln.append([offset, relend])
        tt.append(np.arange(relend - offset) / SRATE)
        for i, rec in enumerate(recs[1:-1], start=1):
            recstart: datetime = af['timeline']['start_time'][ratidx[i]]
            l = af['timeline']['length'][ratidx[i]]
            ln.append([0, l])
            tt.append(np.arange(l) / SRATE + (recstart - start).total_seconds())
        recstart = af['timeline']['start_time'][ratidx[-1]]
        relend = int(SRATE * (end - recstart).total_seconds())
        ln.append([0, relend])
        tt.append(np.arange(relend) / SRATE
                  + timedelta.total_seconds(recstart - start))
    ln = np.asarray(ln)
    lln = np.sum(ln[:, 1] - ln[:, 0])
    if rtype == 'df':
        ret = pd.DataFrame(index=pd.RangeIndex(lln), columns=['t'] + cols)
        ret.loc[:, 't'] = np.concatenate(tt)
    elif rtype == 'ndarray':
        ret = np.empty((lln, len(cols) + 1))
        ret[:, 0] = np.concatenate(tt)
    else:  # This case should be unreachable.
        raise ValueError(f"Invalid return type specified: {rtype=}")
    del tt
    i = 0
    for (ns, ne), rec in zip(ln, recs):
        for n, col in enumerate(cols):
            if rtype == 'df':
                el = af['records'][rec]['source']['raw'][col][ns:ne]
                ret.loc[i:i + ne - ns - 1, col] = el
            elif rtype == 'ndarray':
                ret[i:i + ne - ns, n] = af['records'][rec]['source']['raw'][col][ns:ne]
        i += ne - ns
    if rtype == 'df':
        if index == 't':
            ret.set_index('t', inplace=True)
    return ret
