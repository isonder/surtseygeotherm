from typing import Union
from pathlib import Path
import importlib.metadata as ilibmd
import pandas as pd
from numpy.typing import NDArray

__version__ = ilibmd.version(__package__)
software_entry = {
    'name': "The PVL Surtsey palagonite analysis stack.",
    'author': "The PVL team.",
    'homepage': "https://gitlab.com/isonder/surtseygeotherm",
    'version': __version__
}
versionmap: dict = {
    "runlevels": {
        0: {
            "version": "0.2",
            "creator-files": ["00_save-raw-to-asdf.py"]
        },
        1: {
            "version": "0.3",
            "creator-files": ["10_make-level1-asdf.py"]
        },
        2: {
            "version": "0.4",
            "creator-files": [
                "20_make-timeline.ipynb",
                "21_tccoords.ipynb"
            ]
        },
        3: {
            "version": "0.4",
            "creator-files": [
                "30_clean-mass-signal.ipynb",
                "31_check-mass-signal.ipynb",
                "32_mass-clean-correct.py"
            ]
        }
    },
}
dataloc: Path = Path('.')
str_or_path = Union[str, Path]
df_or_tuple = Union[pd.DataFrame, tuple]
df_or_ndarray = Union[pd.DataFrame, NDArray]

tpath = Path.cwd() / "dataloc"
if tpath.exists():
    with open(tpath) as fd:
        dataloc: Path = Path(fd.read())
TOPLEVEL_SRC: Path = dataloc / "00_surtsey-geotherm.asdf"
SRATE: float = 4.0


class Columns:
    # Some standard column names
    _cjcs: list[str] = ["CJC03", "CJC47", "CJC811", "CJC1215"]
    _temps: list[str] = [f"T{i}" for i in range(16)]
    RAW: list[str] = ['t', 'mass'] + _temps + _cjcs
    ALL: list[str] = RAW + ['masscln']
    DEFAULT: list[str] = ["t", "masscln"] + _temps
