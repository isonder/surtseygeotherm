from surtseygeotherm import __version__, dataloc


class Info:
    """Information object for this package."""

    content: dict[str: str] = {
        'head': 'The Surtsey-Geotherm Package',
        'version': f'Version: {__version__}',
        'start': 'Start time: 2018-06-13 11:24:44',
        'end': 'End time: 2019-10-17 14:04:36',
        'colhead': 'Recorded Data Columns',
        'colmass': 'Mass (in kg).',
        'coltemp': 'T0 ... T15: tempertatures (in °C).',
        'colcjc': "'CJC03', 'CJC47', 'CJC811', 'CJC1215': Cold junction"
                  " compensation readings (in °C)."
    }
    ext_cont: dict[str: str] = {
        'tlexplain':
            f'Raw data is stored as text files in `{str(dataloc.parent)}`. '
            f'Scriptys and notebooks starting with a doubnle digit (`00_`, '
            f'`10_`, ...) copy the data and/or generate often used new data, '
            f'and store them in an asdf structure, located at '
            f'`{str(dataloc)}`.'
    }

    def __init__(self, elaborate: bool = False):
        self.elaborate = elaborate

    def __repr__(self):
        s = [
            f'{self.content["head"]}\n{len(self.content["head"]) * "-"}',
            f'{self.content["version"]}',
            '',
            f'  - {self.content["start"]}',
            f'  - {self.content["end"]}',
            '',
            f'{self.content["colhead"]}',
            f'  - {self.content["colmass"]}',
            f'  - {self.content["coltemp"]}',
            f'  - {self.content["colcjc"]}',
        ]
        return "\n".join(s)

    def _repr_html_(self):
        s = [
            f'<p style="font-size: 120%; font-weight: bold;">{self.content["head"]}</p>',
            f'<p><i>{self.content["version"]}</i></p>'
        ]
        if self.elaborate:
            s += [
                f'<p>{self.ext_cont["tlexplain"]}</p>'
            ]
        s += [
            '<ul>',
            f'  <li>{self.content["start"]}</li>',
            f'  <li>{self.content["end"]}</li>',
            '</ul>'
        ]
        s += [
            f'<b>{self.content["colhead"]}</b><br/>',
            '<ul>',
            f'  <li>{self.content["colmass"]}</li>',
            f'  <li>{self.content["coltemp"]}</li>',
            f"  <li>{self.content['colcjc']}</li>",
            '</ul>'
        ]
        return '\n'.join(s)


class _InfoHolder:
    terse: Info = Info(elaborate=False)
    elaborate: Info = Info(elaborate=True)

    def __repr__(self):
        s = (f"Surtsey-Geotherm Data Analysis. Version: {__version__}.\n"
             f"Attributes:\n"
             f"  - `terse`: Compact package info.\n"
             f"  - `elaborate`: Elaborate package info.")
        return s


info = _InfoHolder()
