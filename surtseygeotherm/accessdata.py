from pathlib import Path
from typing import Union
from datetime import datetime
import numpy as np
from numpy import datetime64 as dt64, timedelta64 as td64
from numpy.typing import NDArray
import pandas as pd
import asdf

from surtseygeotherm import dataloc, Columns, save_1d

tpcdtype = Union[None, float, str, np.datetime64, datetime]
COLS = Columns.DEFAULT
TD64_1S = td64(1, 's')


def create_1hz_record(data: NDArray, record: dict, name: str) -> str:
    """Reduce a column of raw data to a 1 Hz signal and save it into an asdf
    file.

    Parameters
    ----------
    data : NDArrau
        Data to reduce.
    record : dict or asdf tree part
        The record the data column belongs to.
    name : str
        Name of the data column.

    Returns
    -------
        str
    Name of the data column.

    """
    print(f"create_1hz_record: Generating 1Hz data from raw for {name}.")
    last: int | None = len(data) // 4 * 4 - len(data)
    if last == 0:
        last = None
    # print(f'creat_1hz_record: {len(data)=}, {last=}, {data[0:last:4].shape=}')
    d1hz = (data[0:last:4] + data[1:last:4] + data[2:last:4] + data[3:last:4]) / 4
    ttree = {k: record[k] for k in ('start_time', 'end_time', 'record')}
    ttree['runlevel'] = 3
    target = save_1d(
        data=d1hz, loc=dataloc, tree=ttree, name=f"1hz_{name}",
        comment="Downsampled to 1 Hz from raw signal.",
        colname=name,
        runlevel=3
    )
    return target


def td64_to_float(time: td64) -> np.float64:
    return time / TD64_1S


def float_to_td64(time: float) -> td64:
    return td64(int(1e3 * time), "ms")


def get_record(
        rec: str, fmt: str = "df", use_every: int = 1, columns: Union[list, tuple] = COLS,
        start: tpcdtype = None, end: tpcdtype = None, *,
        time_is_index: bool = False, time_is_datetime: bool = False
) -> Union[pd.DataFrame, tuple[NDArray]]:
    """Read a record from disk and return the data.

    The return format is specified by `fnt` and is either a pandas dataframe or
    a numpy array. Optionally, the time column may be formatted as datetime values
    and samples can be skipped.

    Parameters
    ----------
    rec : str
        Record to load from.
    fmt : str
        Data format to create. One of: 'df', 'array'.
    use_every : int
        Only return every `use_every` sample.
    columns : tuple or list or similar
        Columns to read from disk.
    start : float or datetime or str
    end : float or datetime or str
    time_is_index : bool
        Whether to use the time column as index for the DataFrame format.
    time_is_datetime : bool

    Returns
    -------
        pd.DataFrame or tuple[np.ndarray, np.ndarray]
    Contents of the record.
    """
    # Check that all columns are valid
    invalid_cols = [col for col in columns if col not in Columns.ALL]
    if len(invalid_cols) > 0:
        raise ValueError(f"Got invalid columns: {invalid_cols}.")
    # Open the record's asdf file
    rafpath = dataloc / f"{rec}.asdf"
    raf = asdf.open(rafpath, mode='rw')
    raf.find_references()
    startdt: dt64 = np.datetime64(raf['start_time'].isoformat())
    enddt: dt64 = dt64(raf['end_time'].isoformat())
    # Check start and end for consistency, otherwise warn and reset
    if type(start) is not type(end):
        if (isinstance(start, float) and isinstance(end, int)) \
                or (isinstance(start, int) and isinstance(end, float)):
            pass
        raise TypeError(f"start and end must be of type. Got {start=} and {end=}.")
    if start is not None:
        if isinstance(start, str):
            start: dt64 = dt64(start)
            end: dt64 = dt64(end)
            absconstr: bool = True
        elif isinstance(start, datetime):
            start: dt64 = dt64(start.isoformat())
            end: dt64 = dt64(end.isoformat())
            absconstr: bool = True
        elif isinstance(start, float) or isinstance(start, int):
            absconstr: bool = False
        else:
            raise TypeError(f"Got wrong type for start, end: {start=}, {end=}.")
        if time_is_datetime:
            if not absconstr:
                print(f"Coercing start, end ({start}, {end}) to absolute time base.")
                start: dt64 = startdt + float_to_td64(start)
                end: dt64 = startdt + float_to_td64(end)
            if start < startdt:
                print(f"Requested {start=} outside record interval starting at "
                      f"{startdt}. Resetting to {startdt}")
                start: dt64 = startdt
            if end > enddt:
                print(f"Requested {end=} outside record interval ending at "
                      f"{enddt}.  Resetting to {enddt}")
                end: dt64 = enddt
        else:
            if absconstr:
                print(f"Coercing start, end ({start}, {end}) to relative time base.")
                start: float = td64.astype(td64.astype(start - startdt, 's'), float)
                end: float = td64.astype(td64.astype(end - startdt, 's'), float)
            if start < 0:
                print(f"Requested {start=} outside record interval starting at"
                      f" {startdt}. Resetting to 0s.")
                start: float = 0.
            end_s: float | np.float64 = td64_to_float(enddt - startdt)
            if end > end_s:
                print(f"Requested {end=} outside record interval ending at"
                      f" {enddt}. Resetting to {end_s}s.")
                end: float = end_s

    # Identify requested, but missing columns.
    try:
        onehz = raf['1Hz']
    except KeyError:
        raf['1Hz'] = {}
        onehz = raf['1Hz']
    missing = [col for col in columns if col not in onehz.keys()]
    for name in onehz.keys():
        if name in columns:
            if not Path.exists(dataloc / f"{rec}_1hz_{name}.asdf"):
                missing.append(name)
    # Create the missing, but requested entries in the record-level asdf.
    # If the 1 Hz signal is missing on disk, create it.
    for col in missing:
        colpath = dataloc / f"{rec}_1hz_{col}.asdf"
        if not colpath.exists():
            dta = raf['raw'][col] if col != "masscln" else raf['masscln']['masscln']
            create_1hz_record(dta, raf.tree, col)
        onehz[col] = {"$ref": f"{rec}_1hz_{col}.asdf#{col}"}
    if len(missing) > 0:
        raf.write_to(rafpath)
        raf.close()
        raf = asdf.open(rafpath, mode='rw')
        raf.find_references()
        onehz = raf['1Hz']

    # Now actually load the data
    # For now we load the complete time axis to get clean start and end indices.
    t = onehz['t'][:]
    if time_is_datetime:
        t = np.asarray(
            np.asarray(t * 1e3, dtype=int), dtype='timedelta64[ms]'
        ) + startdt
    if start is None:
        startidx = None
    else:
        try:
            startidx = np.where(t <= start)[0][0]
        except IndexError:
            startidx = None
    if end is None:
        endidx = None
    else:
        try:
            endidx = np.where(t >= end)[0][-1]
        except IndexError:
            endidx = None
    t = t[startidx:endidx:use_every]
    if fmt.lower() == 'df':
        ret = pd.DataFrame(index=pd.RangeIndex(len(t)))
        for col in columns:
            if col == 't':
                ret[col] = t
            else:
                ret[col] = onehz[col][startidx:endidx:use_every]
        if time_is_index:
            ret.set_index('t', inplace=True)
    elif fmt.lower() == 'array':
        retar = np.empty((len(t), len(columns) - 1), dtype=float)
        i = 0
        for col in columns:
            if col != 't':
                retar[:, i] = onehz[col][startidx:endidx:use_every]
                i += 1
        ret = (t, retar)
    else:
        raise ValueError(f"Unknown data format: {fmt=}.")
    return ret
