import numpy as np
from numpy.typing import ArrayLike, NDArray
from pandas import DataFrame
from scipy.optimize import curve_fit
import ipywidgets as widgets
from IPython.display import display


def mass_model01(
        time: NDArray,
        a0: float, f0: float, f2: float, md1: float, dt1: float, dt2: float,
        pw: float, rate: float, m0: float
) -> NDArray:
    """A model for mass time dependency.

    See the 33_mass-time-model.ipynb notebook for parameter explanation.

    Returns
    -------
        NDArray
    Mass time dependency.
    """
    ret = np.empty_like(time)
    t0 = time[0]
    dm1 = a0 * (1. - np.exp(-f0 * dt1))
    eps2 = np.exp(-f2 * (time[-1] - dt1 - dt2 - t0) ** pw)
    a2 = (md1 * dt2 + dm1) / (1 - eps2)
    idx = time <= dt1 + time[0]  # _____________________________________ t0 <= t <= t1
    if np.any(idx):
        ret[idx] = a0 * (1. - np.exp(-f0 * (time[idx] - t0))) - rate * (time[idx] - t0) + m0
    idx = np.logical_and(~idx, time <= dt2 + dt1 + t0)  # ___________ t1 <= t <= t2
    if np.any(idx):
        ret[idx] = md1 * (time[idx] - dt1 - t0) + dm1 - rate * (time[idx] - t0) + m0
    idx = time >= dt2 + dt1 + t0  # _________________________________ t2 <= t <= t3
    if np.any(idx):
        ret[idx] = a2 * (np.exp(-f2 * (time[idx] - dt2 - dt1 - t0) ** pw) - eps2) \
                   - rate * (time[idx] - t0) + m0
    return ret


type mftuple = tuple[list[float], list[float]]
MASSFIT_BOUNDS: mftuple = (
    [1e-1, 1e-4, 1e-6, 5e-6, 50, 350, 0.2],
    [1, 1e-1, 1e-1, 1e-3, 10000, 100000, 1.3]
)


def massfit(
        data: DataFrame, refs: DataFrame, params: tuple | list,
        bounds: mftuple = None
) -> NDArray:
    """Fit a mass time dependent model to data using the intervals specified
    in `refs`.

    Parameters
    ----------
    data : DataFrame
        Data to fit model to.
    refs : DataFrame
        Interval start reference values.
    params : list
        Initial guess of model parameters. They must be in the order:
        a0, f0, f2, md1, dt1, dt2, pw
    bounds : mftuple, optional, default: MASSFIT_BOUNDS
        Upper and lower bounds of model parameters.

    Returns
    -------
        NDArray
    Optimized model parameters.
    """
    if bounds is None:
        bounds = MASSFIT_BOUNDS
    pars = np.empty((len(refs), 9))
    out = widgets.Output(layout={'border': '1px solid black'})
    display(out)
    with out:
        ln = len(refs) - 1
        prog = widgets.IntProgress(
            value=0, min=0, max=len(refs), description=f"0 of {ln}.",
            bar_style='info'
        )
        display(prog)
        for i, (t, m) in enumerate(zip(refs.index.values, refs.masscln.values)):
            if i == len(refs) - 1:
                break
            print(f"{i:02d}: t={t:.1e}, m={m:.3f},", end=" ")
            rate = - (refs.iloc[i + 1].masscln - m) / (
                refs.index[i + 1] - t
            )
            sel = data.loc[t:refs.index[i + 1]]
            xx = sel.index.values
            yy = sel.masscln.values

            def func(x, a0, f0, f2, md1, dt1, dt2, pw):
                return mass_model01(x, a0, f0, f2, md1, dt1, dt2, pw, rate, m)
            res = curve_fit(func, xx, yy, p0=params, bounds=bounds)
            pars[i, :7] = res[0]
            pars[i, 7:] = rate, m
            prog.value = i + 1
            prog.description = f"{i + 1} of {ln}."
            p = pars[i]
            print(
                f"a0={p[0]:.1e}, f0={p[1]:.1e}, f2={p[2]:.1e}, md1={p[3]:.2e}, "
                f"dt1={p[4]:.0f}, dt2={p[5]:.0f}, pw={p[6]:.2f}, rt={rate:.1e}"
            )
        prog.close()
    return pars


def find_local_temp_extremes(
        temp: NDArray, w: int, steps: int = 10, trustlevel: int = 3
) -> dict:
    """Find local minima and maxima, based on a windowed search of `w` samples.

    This function is meant to work mostly for the temperature data. It does not
    rely on a time (or other x-) axis at all.

    Parameters
    ----------
    temp : NDArray
        A 1D array of temperature data.
    w : int
        Window size. Unit is the number of samples (not time!).
    steps : int, optional, default: 10
        Number of steps to divide w into.
    trustlevel : int, optional, default: 3
        How often a sample needs to be identified as extemum, until it is
        labeled as one.

    Returns
    -------
        dict
    A dictionary of the following form:
    ```
    {"min": {"index": ..., "values": ...}, "max": {"index": ..., "values": ...}}
    ```
    """
    step: int = w // steps
    if w % steps != 0:
        w = step * steps
        print(f"Adjusting window to {w} samples.")
    end, ln = w, len(temp)
    mins, maxs = {}, {}
    while end < ln:
        start = end - w
        sel = temp[start:end]
        mn, mx = sel.min(), sel.max()
        imn, imx = sel.argmin() + start, sel.argmax() + start
        if imn in mins.keys():
            mins[imn][0] += 1
        else:
            mins[imn] = [1, mn]
        if imx in maxs.keys():
            maxs[imx][0] += 1
        else:
            maxs[imx] = [1, mx]
        end += step
    imns, vmns = [], []
    for idx, el in mins.items():
        if el[0] >= trustlevel:
            imns.append(idx)
            vmns.append(el[1])
    imxs, vmxs = [], []
    for idx, el in maxs.items():
        if el[0] >= trustlevel:
            imxs.append(idx)
            vmxs.append(el[1])
    return {
        'min': {'index': np.asarray(imns), 'values': np.asarray(vmns)},
        'max': {'index': np.asarray(imxs), 'values': np.asarray(vmxs)}
    }
