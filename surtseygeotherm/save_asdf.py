from copy import deepcopy
import asdf
from numpy.typing import NDArray
from typing import Union
from pathlib import Path
from surtseygeotherm import software_entry, versionmap

str_or_path = Union[str, Path]
str_or_none = Union[str, None]


def save_1d(
        data: NDArray, loc: str_or_path, tree: dict, name: str = "noname",
        comment: str = "", runlevel: int = -1, creator: str_or_path = "",
        colname: str_or_none = None, verbose: bool = False
) -> str:
    """

    Parameters
    ----------
    data : NDArray
    loc : str or Path
    name : str
        Name of the column (1d array).
    comment : str
        The comment will be saved in the asdf history entry.
    tree : dict
        Existing metadata to save in the file.
    runlevel : int
        The runlevel determines some version/formatting info.
    creator : str or Path
        Script that created the column (or called this function).
    colname : str or None, optional
        If given, use this as the main data item's identifier instead of `name`.
    verbose : bool
        Whether to print a status message.

    Returns
    -------

    """
    assert runlevel > -1, (
        f"save_1d: Will not export data column {name} without a valid runlevel"
        f" (given {runlevel=})."
    )
    if colname is None:
        colname = name
    target = loc / f"{tree['record']}_{name}.asdf"
    afile = asdf.AsdfFile(tree=tree)
    afile[colname] = data
    afile['runlevel'] = runlevel
    vinfo = deepcopy(versionmap['runlevels'][runlevel])
    vinfo['creator-files'] = [Path(creator).name, ]
    afile['versioninfo'] = vinfo
    afile.add_history_entry(description=comment, software=software_entry)
    afile.write_to(
        target, all_array_storage="internal", all_array_compression="zlib")
    afile.close()
    if verbose:
        print(f"save_1d: Done with {str(target)}.", flush=True)
    return f"{target.stem}.asdf"
