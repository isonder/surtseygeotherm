"""Collection of data analysis notebooks, helper functions and raw data cleaner
methods for the SurtseyGeotherm experiment.
"""
from .globals import (
    dataloc, TOPLEVEL_SRC, SRATE, Columns,
    str_or_path, df_or_tuple, df_or_ndarray,
    software_entry, __version__, versionmap
)
from .info import info
from .massfilter import filter_mass
from .save_asdf import save_1d
from .datetimemap import records_from_timespan, load
from .accessdata import get_record
from .analysis import (
    mass_model01, massfit, MASSFIT_BOUNDS, find_local_temp_extremes
)
