import numpy as np
from numpy.typing import NDArray


def vprint(msg: str, verbose: bool, **kwargs):
    if verbose:
        print(msg, **kwargs)


def filter_mass(
        data: NDArray,
        levels: tuple = tuple(range(15)),
        verbose: bool = False
) -> NDArray:
    """Applies a filter to fix the noisy mass signal.

    That signal was most likely distorted by a weak serial connection. Strings
    arriving at the logger were truncated to either integers or too short
    floats. Below routine searches for truncation levels and replaces bogus
    with interpolated values.

    Parameters
    ----------
    data : NDArray
        Data to filter.
    levels : tuple
        A list of levels for which to check. May be integers or floats.
    verbose : bool
        Whether to print status messages.

    Returns
    -------
        NDarray
    Cleaned mass signal.
    """
    cleaned = data.copy()
    s = []
    none_found = []
    for lev in levels:
        # print(f"{lev=}")
        idx, = np.where(np.isclose(cleaned, lev, atol=1e-4))
        if len(idx) == 0:
            none_found.append(lev)
            continue
        if idx[0] == 0:
            idx = idx[1:]
        ioi, = np.where(cleaned[idx - 1] - cleaned[idx] > 1e-3)
        idx4 = []
        for i in idx[ioi]:
            k = i + 1
            try:
                while np.isclose(cleaned[k], lev, atol=1e-4):
                    k += 1
                idx4.append([i, k - 1])
            except IndexError:
                pass
        if len(idx4) == 0:
            none_found.append(lev)
            continue
        else:
            s.append(f'Level {lev}: {len(idx4)} occurrences.')

        idx4 = np.asarray(idx4)
        for before, after in zip(idx4[:, 0] - 1, idx4[:, 1] + 1):
            y1, y2 = cleaned[[before, after]]
            x1, x2 = before, after
            slope = (y2 - y1) / (x2 - x1)
            off = (x2 * y1 - x1 * y2) / (x2 - x1)
            t = np.arange(before + 1, after)
            cleaned[before + 1:after] = slope * t + off
    vprint(f"No occurrence of levels {none_found}.", verbose)
    vprint("\n".join(s), verbose)
    return cleaned
