from pathlib import Path
import asdf
import matplotlib.pyplot as plt
import numpy as np
from numpy.typing import NDArray

import surtseygeotherm as sgt


def docplot(
        t: NDArray, mraw: NDArray, mfilt: NDArray, mcorr: NDArray | None, grf: Path,
        title: str = None, simple: bool = False
):
    if simple:
        fig, ax1 = plt.subplots(layout='constrained')
        fig.set_size_inches(8, 6)
    else:
        fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True, layout='constrained')
        ax2.plot(t, mfilt, zorder=-1, label='filtered')
        ax2.plot(t, mcorr, zorder=1, label='corrected')
        ax2.set(xlabel=r'$t\ /\ \mathrm{s}$', ylabel=r'$m\ /\ \mathrm{kg}$')
        ax2.legend()
        fig.set_size_inches(8, 10)
    if title is not None:
        fig.suptitle(title)
    ax1.plot(t, mraw, zorder=-1, label='raw')
    ax1.plot(t, mfilt, zorder=1, label='filtered')
    ax1.set(xlabel=r'$t\ /\ \mathrm{s}$', ylabel=r'$m\ /\ \mathrm{kg}$',)
    ax1.legend()
    fig.savefig(grf, dpi=300)


def cleanup_1hz_masssig(af: asdf.AsdfFile):
    """Remove mass signal from the '1hz' collection, and delete corresponding
    data column file from disk. (If applicable.)
    """
    if "1Hz" in af.keys():
        if "masscln" in af["1Hz"].keys():
            del af["1Hz"]["masscln"]
            pth = sgt.dataloc / f"{af['record']}_1hz_masscln.asdf"
            if pth.exists():
                pth.unlink()


def plot_and_save(
        af: asdf.AsdfFile,
        t: NDArray, mraw: NDArray, mfilt: NDArray, mcorr: NDArray | None,
        rec: str, simple_plot: bool = False, comment: str = "Cleaned."
):
    graph_file = Path("datacheck") / f"{rec}.png"
    docplot(t, mraw, mfilt, mcorr, graph_file, rec, simple=simple_plot)
    dta: NDArray = mcorr if mcorr is not None else mfilt
    sgt.save_1d(
        data=dta, loc=sgt.dataloc,
        tree=dict(record=rec, start_time=af['start_time'], end_time=af['end_time']),
        name="masscln", comment="Cleaned mass signal.",
        runlevel=3, verbose=True
    )
    af['masscln']['status'] = comment
    cleanup_1hz_masssig(af)
    af.update()


def correct_a023025(force_processing: bool = False):
    record = 'Surtsey-A-023-25'
    af = asdf.open(sgt.dataloc / f"{record}.asdf", mode='rw')
    if force_processing or 'status' not in af['masscln'].keys():
        print(f"Correcting mass signal of {record}.")
        mraw = af['raw']['mass'][:]
        traw = af['raw']['t'][:]
        # filter mass signal at first decimal level
        mfilt = sgt.filter_mass(
            mraw, levels=tuple(np.arange(10, 11.1, 0.1)), verbose=True)
        plot_and_save(
            af, traw, mraw, mfilt, mcorr=None, rec=record, simple_plot=True,
            comment="Cleaned; inversion correction not necessary."
        )
        print("")
    af.close()


def correct_a024026(force_processing: bool = False):
    record = 'Surtsey-A-024-26'
    # get raw mass signal and time
    af = asdf.open(sgt.dataloc / f"{record}.asdf", mode='rw')
    if force_processing or 'status' not in af['masscln'].keys():
        print(f"Correcting mass signal of {record}.")
        mraw = af['raw']['mass'][:]
        # filter mass signal at first decimal level
        mfilt = sgt.filter_mass(mraw, levels=tuple(np.arange(20) / 10 + 9), verbose=True)
        plot_and_save(
            af, af['raw']['t'], mraw, mfilt, mcorr=None, rec=record, simple_plot=True,
            comment="Cleaned; inversion correction not necessary."
        )
        print("")
    af.close()


def correct_a025027(force_processing: bool = False):
    record = 'Surtsey-A-025-27'
    af = asdf.open(sgt.dataloc / f"{record}.asdf", mode='rw')
    if force_processing or 'status' not in af['masscln'].keys():
        print(f"Correcting mass signal of {record}.")
        mraw = af['raw']['mass'][:]
        mfilt = sgt.filter_mass(mraw, levels=tuple(np.arange(9, 11.2, 0.1)), verbose=True)
        plot_and_save(
            af, af['raw']['t'], mraw, mfilt, mcorr=None, rec=record, simple_plot=True,
            comment="Cleaned; inversion correction not necessary."
        )
        print("")
    af.close()


def correct_a155161(force_processing: bool = False):
    record = 'Surtsey-A-155-161'
    # get raw mass signal and time
    af = asdf.open(sgt.dataloc / f"{record}.asdf", mode='rw')
    if force_processing or 'status' not in af['masscln'].keys() \
        or ('status' in af['masscln'].keys()
            and af['masscln']['status'] == 'Cleaned, likely needs correction.'):
        print(f"Correcting mass signal of {record}.")
        mraw = af['raw']['mass'][:]
        traw = af['raw']['t'][:]
        # filter mass signal at first decimal level
        mfilt = sgt.filter_mass(mraw, levels=tuple(np.arange(10) / 10), verbose=True)
        # correct inverted mass signal where necessary
        start = np.where(traw > 1.1327e5)[0][0]
        end = np.where(traw > 1.18905e5)[0][0]
        mcorr = mfilt.copy()
        mcorr[start:end] *= -1
        start = np.where(traw > 1.19447e5)[0][0]
        end = np.where(traw > 1.51615e5)[0][0]
        mcorr[start:end] *= -1
        plot_and_save(
            af, traw, mraw, mfilt, mcorr, rec=record, simple_plot=False,
            comment="Cleaned and inversion corrected mass signal."
        )
        print("")
    af.close()


def correct_a156162(force_processing: bool = False):
    record = "Surtsey-A-156-162"
    af = asdf.open(sgt.dataloc / f"{record}.asdf", mode='rw')
    if force_processing or 'status' not in af['masscln'].keys() \
        or ('status' in af['masscln'].keys()
            and af['masscln']['status'].startswith('Cleaned, likely needs correction.')):
        print(f"Correcting mass signal of {record}.")
        mraw = af['raw']['mass'][:]
        traw = af['raw']['t'][:]
        mcln = sgt.filter_mass(mraw, levels=(0., .1, .2, .3), verbose=True)
        mcorr = mcln.copy()
        start = np.where(traw >= 1.35615e5)[0][0]
        end = np.where(traw >= 1.395293e5)[0][0]
        mcorr[start:end] *= -1
        start = np.where(traw >= 1.40557e5)[0][0]
        end = np.where(traw >= 1.58854e5)[0][0]
        mcorr[start:end] *= -1
        plot_and_save(
            af, traw, mraw, mcln, mcorr, rec=record, simple_plot=False,
            comment="Cleaned and inversion corrected mass signal."
        )
        print("")
    af.close()


def correct_a157163(force_processing: bool = False):
    record = 'Surtsey-A-157-163'
    af = asdf.open(sgt.dataloc / f"{record}.asdf", mode='rw')
    if force_processing or 'status' not in af['masscln'].keys():
        print(f"Correcting mass signal of {record}.")
        mraw = af['raw']['mass'][:]
        traw = af['raw']['t'][:]
        mcln = sgt.filter_mass(mraw, levels=tuple(np.arange(0, .7, .1)), verbose=True)
        mcorr = mcln.copy()
        start = np.where(traw >= 184910)[0][0]
        end = np.where(traw >= 186094)[0][0]
        mcorr[start:end] *= -1
        start = np.where(traw >= 187428)[0][0]
        end = np.where(traw >= 241235)[0][0]
        mcorr[start:end] *= -1
        plot_and_save(
            af, traw, mraw, mcln, mcorr, rec=record, simple_plot=False,
            comment="Cleaned and inversion corrected mass signal."
        )
        print("")
    af.close()


if __name__ == '__main__':
    import argparse
    reg = {
        'surtsey-a-023-25': correct_a023025,
        'surtsey-a-024-26': correct_a024026,
        'surtsey-a-025-27': correct_a025027,
        'surtsey-a-155-161': correct_a155161,
        "surtsey-a-156-162": correct_a156162,
        "surtsey-a-157-163": correct_a157163
    }
    alts = {}
    for k in reg.keys():
        alts[k[8:]] = k
        alts[k[10:]] = k
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f', '--force', action=argparse.BooleanOptionalAction,
        dest="force_recreate", default=False,
        help="Force re-processing of raw signals and storing in the asdf database "
             "as new file. By default this is only done if a columns file is "
             "missing."
    )
    parser.add_argument(
        '-r', '--records', action='extend', nargs='+', type=str,
        help="Records to process. If none is provided, all are processed."
    )
    args = parser.parse_args()
    records: list[str] = args.records
    if records is None:
        records = list(reg.keys())
    else:
        records = [rec.lower() for rec in records]
    force_recreate: bool = args.force_recreate
    for rec in records:
        try:
            reg[rec].__call__(force_processing=force_recreate)
        except KeyError:
            reg[alts[rec]].__call__(force_processing=force_recreate)
