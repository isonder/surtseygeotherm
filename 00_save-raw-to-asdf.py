"""Read all raw data and store them in a suitable asdf structure as `asdf`
folder in the raw data's directory.
"""
import pandas as pd
from pathlib import Path
from datetime import datetime
import asyncio
from concurrent.futures import ProcessPoolExecutor
from os import sched_getaffinity
from copy import deepcopy

from surtseygeotherm import dataloc, save_1d, versionmap

THIS_FILES_NAME = f"{Path(__file__).stem}.py"
NCPUS = 2 * len(sched_getaffinity(0))
VINFO = deepcopy(versionmap['runlevels'][0])
VINFO['creator-files'] = [THIS_FILES_NAME]


async def handle_source(
    source: Path,
    loop: asyncio.AbstractEventLoop,
    executor: ProcessPoolExecutor
) -> tuple:
    # Last modified time (hopefully when the logger closed the file)
    end_dt = datetime.fromtimestamp(source.lstat().st_mtime)
    # load data
    data = pd.read_csv(source, sep='\t', index_col=0)
    data.columns = [s.split('/')[0] for s in data.columns]
    data.index.name = 't'
    record = source.name.split('_')
    record = "".join(record[1:-1])
    tree = {
        'record': record,
        'end_time': end_dt
    }
    units = {'t': 's', 'mass': 'kg'}
    for col in data.columns:
        if col != "mass":
            units[col] = "°C"
    tree['raw'] = {
        'columns': list(data.columns),
        'units': units,
        'versioninfo': VINFO
    }
    asdfloc = source.parent / "asdf"
    tasks = []
    for col in ['t'] + tree['raw']['columns']:
        xtree = {k: tree[k] for k in ['record', 'end_time']}
        xtree['versioninfo'] = versionmap['runlevels'][0]
        cdata = data.index.values if col == 't' else data[col].values
        comment = f"Copied over from raw text file."
        tasks.append(loop.run_in_executor(
            executor, save_1d,
            *(cdata, asdfloc, xtree, col, comment, 0, THIS_FILES_NAME, True)
        ))
    await asyncio.gather(*tasks, return_exceptions=True)
    ret = asdfloc / f"{record}.asdf"
    return ret, record, end_dt, len(data)


async def main():
    sources = list(dataloc.parent.glob('*T-m.txt'))
    sources.sort()
    loop = asyncio.get_event_loop()
    xcutor = ProcessPoolExecutor(max_workers=NCPUS)
    tasks, results = [], []
    for i, p in enumerate(sources):
        tasks.append(loop.create_task(handle_source(p, loop, xcutor)))
        if i % 4 == 0:
            for res in await asyncio.gather(*tasks, return_exceptions=True):
                results.append(res)
            tasks = []
    if len(tasks) > 0:
        for res in await asyncio.gather(*tasks, return_exceptions=True):
            results.append(res)


if __name__ == '__main__':
    assert dataloc.exists(), \
        f"Cannot find main data folder {dataloc}."
    print(f"Using {NCPUS} CPUs.")
    asyncio.run(main())
